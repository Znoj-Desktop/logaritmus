/*
 * Soubor:  logaritmus.cpp
 * Datum:   2011/12/01
 * Autor:   
 * Projekt do ZP
 */

//pripojeni knihovny jazyka c++ pro vstup/vystupni operace
#include <iostream>
//knihovna je pouzita pro vypocet logaritmu z malych cisel
#include <math.h>
using namespace std;

//konstanta eulerova cisla
const long double e = 2.718281828459045235360287471352662497757247093699959574966967627724076630353547594571382178525166427427466391932003;
//konstanta prirozeneho logaritmu cisla deset -> ln(10)
const long double lndeseti = 2.302585092994045684017991454684364207601101488628772976033327900967572609677352480235997205089598298341967784042286;


/** \brief cte a kontroluje parametry prikazoveho radku
 *
 * Pokud neni zadan parametr, nebo pokud je zadan parametr "--h", 
 * pak se vola funkce pro vypis napovedy a program uspesne skonci.
 * Poud je zadan 1 parametr, pak funkce nic neudela.
 * Pokud je zadan jiny nez drive uvedeny pocet parametru (2 a vice),
 * pak se vytiskne prislusne erroroveho hlaseni
 *
 * @param argc udava pocet zadanych parametru se kterymi je program spusten
 * @param *argv[] je pole ukazatelu ukazujicich na pamet ve ktere je ulozen obsah parametru prikazoveho radku
 */
void kontrola_parametru(int argc, char *argv[])
{
	//pokud neni zadan zadny parametr, nebo prave jeden "--h", pak se vytiskne napoveda
	if((argc == 1) || ((argc == 2) && (strcmp("--h", argv[1]) == 0)))
	{
		cout << "Napoveda:" << endl;
		cout << "Kdyz se zada --h nebo nic, tak se vytiskne napoveda" << endl;
		cout << "Kdyz se zada jeden parametr a je to cele cislo, pak se z nej vypocita prirozeny logaritmus" << endl;
		cout << "Kdyz se zada parametr ktery neni cislo, nebo kdyz se zada vice nez jeden parametr, pak se vytiskne chyba" <<endl;
		system("PAUSE");
		exit(EXIT_SUCCESS);
	}

	//pokud je zadan jiny pocet parametru, pak se jedna o chybu
	else if (argc > 2)
	{
		cout << "zadano moc parametru" << endl;
		exit(EXIT_FAILURE);
	}
} 


/** \brief kontroluje a zpracovava parametry
 *
 * Prevadi parametry prikazoveho radku na cislice, kontroluje jestli se jedna o cislice - pokud ano,
 * pak je v pripade prvniho parametru uklada v puvodnim poradi do pole zadaneho 3. parametrem funkce.
 * V pripade chyby (pokud je v parametrech jiny znak nez cislice), tak je uvolneno pole a vytisknut error
 * @param argc udava pocet zadanych parametru se kterymi je program spusten
 * @param *argv[] je pole ukazatelu ukazujicich na pamet ve ktere je ulozen obsah parametru prikazoveho radku
 * @param *pole ukazatel na dynamicky alokovanou pamet do ktere se zapisi cisla parametru prikazove radky
 * @param MAX udava pocet znaku parametru prikazove radky
 */

void zpracovani_parametru(int argc, char *argv[], int *pole, const unsigned long long int MAX)
{
	int cislo = 0;
	char zbytek[2];
	//cyklus nacita cisla do promenne cislo a do pole zbytek nacita to, co cislo neni po jednotlivych cislicich
	for(int i = 0; i < MAX; i++)
	{
		sscanf(argv[1]+i, "%1d", &cislo);
		sscanf(argv[1]+i, "%c", zbytek);
		//pokud je na vstupu jiny znak nez cislo, pak je program ukoncen
		//uvolni se pamet a na stderr se vytiskne chybove hlaseni
		if(!isdigit(zbytek[0]))
		{
			free(pole);
			cout << "NENI CISLO" << endl;
			exit(EXIT_FAILURE);
		}
		pole[i] = cislo;
	} //konec for(int i = 0; i < MAX; i++)
} 

/** \brief vypocita logaritmus
 *
 * Vypocita logaritmus podle vzorce ln(x) + ln(1 + e^(ln(y)-log(x))), ln(y) je o rad vyssi cislo nez x
 * napriklad u cisla 15 je x = 5 a y = 10. Ln(y) lze tedy rozepsat jako ln(z) + nul*ln(10), kde nul udava
 * pocet nul kolik cislo y obsahuje a z je zbytek po deleni nul-teho nasobku deseti. Tedy pro cislo
 * 3000 je z=3 a nul=3, redukuje se tedy pocet cislic se kterymi se musi pracovat. Kazda cislice se
 * vypocita jako soucet logaritmu vsech cislic ktere jsou nizsiho radu.
 *
 * @param pole[] pole ve kterem je ulozeno cislo zadane jako parametr prikazoveho radku
 * @param cislic udava pocet cislic v cisle ulozenem v prvnim parametru se kterym se tato funkce vola
 */
long double logaritmus(int pole[], unsigned long int cislic)
{
	//promenna aktualni cislice je zde proto, protoze je nutne vlozit do funkce log()
	//cislo stejneho typu jako je jeho navratova hodnota.
	long double aktualni_cislice = pole[cislic-1];
	long double ln = log(aktualni_cislice);
	long double exponent = 0;
	long double vysledek_po_umocneni;
	//0 = jednotky, 1 nula = desitky, 2 nuly = stovky, 3 nuly = tisice...
	int nul = 1;
	cout << "aktualne zpracovavana cislice: " << aktualni_cislice << endl;
	cout << "exponent: " << exponent << endl;
	cout << "ln: " << ln << endl;
	//cyklus ktery projde zprava vsechny cisla v poli
	for(int i = (cislic-2); i >= 0; i--)
	{
		aktualni_cislice = pole[i];
		cout << "aktualne zpracovavana cislice: " << aktualni_cislice << endl;
		exponent = nul*lndeseti + log(aktualni_cislice) - ln;
		cout << "exponent: " << exponent << endl;
		vysledek_po_umocneni = pow(e, exponent);
		cout << "vysledek po umocneni: " << vysledek_po_umocneni << endl;
		ln = ln + log(1+vysledek_po_umocneni);
		cout << "log(1+vysledek_po_umocneni): " << log(1+vysledek_po_umocneni) << endl;
		cout << "prubezny vysledek: " << ln << endl;
		nul++;
	}
	
	return ln;
}

/** \brief vypis vysledku v jazyce html
 *
 * Zapise do souboru, jehoz odkaz je zadan prvnim parametrem se kterym je funkce volana, HTML kod, ve kterem bude
 * zadane cislo a vysledek.
 *
 * @param *soubor ukazatel na soubor do ktereho chceme HTML kod zapsat
 * @param *vstup ukazatel na cislo ze ktereho program pocita odmocninu. Je to ukazatel na 1. parametr prikazoveho radku
 * @param cislic_v_odmocnine udava pocet cislic zadaneho cisla 1. parametrem z prikazoveho radku
 * @param vysledek prirozeny logaritmus ze zadaneho cisla
 */
void vypis (FILE *soubor, char *vstup, unsigned long long int cislic_v_odmocnine, long double vysledek)
{	
	fprintf(soubor, "<HTML>\r\n");
	fprintf(soubor, "<HEAD>\r\n");
	fprintf(soubor, "     <TITLE>ZP - P�irozen� logaritmus; Martin Stuska</TITLE>\r\n");
	fprintf(soubor, "</HEAD>\r\n");
	fprintf(soubor, "<BODY BGCOLOR=\"#0BB000\">\r\n");
	fprintf(soubor, "<CENTER> <H1> P�irozen� logaritmus </H1>\r\n");
	fprintf(soubor, "<H3> ln(%s) = <u>%lf</u> <br>\r\n", vstup, vysledek);
	fprintf(soubor, "</CENTER> \r\n");
	fprintf(soubor, "<H6>tato str�nka je vygenerov�na programem <i>logaritmus.cpp</i> <br>\r\n");
	fprintf(soubor, "</BODY>\r\n");
	fprintf(soubor, "<HTML>\r\n");
} 


int main(int argc,char** argv)
{
	kontrola_parametru(argc, argv);
	const unsigned long long int MAX = strlen(argv[1]);
	long double ln = 0;
	
	//dynamicka alokace poli
	//do pole "pole" se nejdrive nacte cislo ze vstupu, ze ktereho
	//ma byt pocitana odmocnina a na konci programu v nem bude vysledek.
	int *pole;
	pole = (int *) malloc (sizeof(int) * (MAX+2));
	//pole2 slouzi jako pomocne pole pro prubezne vypocty

	
	//obe pole inicializovany na -1 - tato hodnota slouzi jako zarazka, jelikoz
	//0 je platna cislice
	for(int i = 0; i < MAX; i++)
	{
		pole[i] = -1;
	}

	//funkce, ktera spousti podprogram kontrolujici spravnost parametru zadanych z prikazoveho radku
	zpracovani_parametru(argc, argv, pole, MAX);

	//odmocnina nic nevraci, protoze vysledek bude v poli, ve kterem je zadane cislo
	//je to z duvodu setreni pameti
	ln = logaritmus(pole, MAX);

	FILE *soubor;
	//otevreni souboru
	soubor = fopen("logaritmus.html", "w");

	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor == NULL)
	{
		cout << "chyba pri otvirani souboru" << endl;
		free(pole);
		exit(EXIT_FAILURE);
	}

	//vypis do souboru HTML
	vypis(soubor, argv[1], MAX, ln);
	
	//vypis na standardni vystup (stdout)
	//-----------------------------------------------------------------------------------------------------------------
	//vypis vysledku
	printf("vysledek: %lf\r\n", ln);
	//------------------------------------------------------------------------------------------------------------------

	//uzavreni souboru
	fclose(soubor);
	//uvoln�n� pole 
	free(pole);
	system("PAUSE");
	return 0;
	
}