### **Description**
Program vypocita logaritmus podle vzorce ln(x) + ln(1 + e^(ln(y)-log(x))), ln(y) je o rad vyssi cislo nez x napriklad u cisla 15 je x = 5 a y = 10. Ln(y) lze tedy rozepsat jako ln(z) + nul*ln(10), kde nul udava pocet nul kolik cislo y obsahuje a z je zbytek po deleni nul-teho nasobku deseti. Tedy pro cislo 3000 je z=3 a nul=3, redukuje se tedy pocet cislic se kterymi se musi pracovat. Kazda cislice se vypocita jako soucet logaritmu vsech cislic ktere jsou nizsiho radu.

---
### **Technology**
C++

---
### **Year**
2011

---
### **Screenshot**
![](./README/screenshot.png)